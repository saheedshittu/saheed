# Special Needs #

## Definitions ##

Extract from [Wikipedia - Special Needs](https://en.wikipedia.org/wiki/Special_needs):

> In the United States, special needs is a term used in clinical diagnostic [sic] and functional development to describe individuals who require assistance for disabilities that may be medical, mental, or psychological.

Extracts from [Wikipedia - Special Education](https://en.wikipedia.org/wiki/Special_education):

> Special education (also known as special needs education, aided education, exceptional education or Special Ed) is the practice of educating students with special educational needs in a way that addresses their individual differences and needs.

> Common special needs include learning disabilities (such as dyslexia), communication disorders, emotional and behavioral disorders (such as ADHD and ADD), physical disabilities (such as Brittle Bone Disease, Cerebral Palsy, Muscular Dystrophy, Spinal Bifida, and Frederich's Ataxia), and developmental disabilities (such as autism spectrum disorders and intellectual disability).[1] Students with these kinds of special needs are likely to benefit from additional educational services such as different approaches to teaching, the use of technology, a specifically adapted teaching area, or a resource room.

## University Resources ##

### Disability Services ###

The primary resource for students (and faculty/staff) with special needs on campus is the Disability Services office, or "DS": https://www.etsu.edu/students/ds/

Students seeking accomodations, assistance, and/or resources through DS are first required to go through an intake process where the DS uses an interview, staff observations, and third party documentation to evaluate the student applying for assistance. That process is outlined here: https://www.etsu.edu/students/ds/student/eligibility.php

Once registered, a student may be eligible for accomodations such as (but not limited to):

- Classroom
    + specific seating
    + recording lectures
    + receiving copies of presentations
    + having a note taker
- Testing
    + alternative answer sheets
    + oral testing
    + low distraction testing room
    + extended test time
- Attendance
    + Coordination between student, instructor, and DS to address attendance concerns

**Location:** D.P. Culp Center, Room 326

#### Anecdotes from another institution ####

Based on the experiences of my friends another institutions, navigating a university's disability services system can be a real challenge. Depending on the nature of a student's disability, it may be that the difficulty of getting accomodations matches or outweighs some of the difficulties imparted by their disability. I personally know people from my undergraduate institution with ADHD and ASD diagnoses who were elgibile for testing accomodations, but rarely used them because coordinating with the disability resource center wasn't worth it. (This does _not_ mean that the DS at ETSU is necessarily similar, but I do have a prexisting bias.)

### Student Support Services ###

https://www.etsu.edu/academicaffairs/trio/sss/default.php

Student Support Services (SSS) offers free tutoring for general education courses, academic counseling, career counseling, personal counseling, and workshops to students meeting specific criteria:

1. ALL of:
    1. Working on their first bachelor's degree
    2. Attend ETSU
    3. be a U.S. citizen (or receive federal finanical assistance)
2. and, ANY of:
    1. First-generation student
    2. Income eligibility
    3. Documented disability

Tutoring: 
https://www.etsu.edu/academicaffairs/trio/sss/tutoring.php

Other (counseling, workshops): https://www.etsu.edu/academicaffairs/trio/sss/services.php

**Location:** D.P. Culp Center, Room 318

#### TRIO ####

SSS is one of ETSU's federally funded TRIO programs: "Federal outreach and student services programs designed to identify and provide services for individuals from disadvantaged backgrounds."

https://www.etsu.edu/academicaffairs/trio/


### Center for Academic Achievement ###

https://www.etsu.edu/uged/cfaa/

The Center for Academic Achievement (CFAA) is composed of Learning Services, Testing Services, and the Office of Assessment. Mission statement:

> The mission of the CFAA is to present students with opportunities to learn and demonstrate their learning in a secure and supportive environment that encourages creative thinking, collaborative learning, and self-direction.

#### Tutoring ###

https://www.etsu.edu/uged/cfaa/learning/default.php

Offer tutoring for walk-ins and by appointment, free for all ETSU students, both for individuals and in groups.

- [Individual tutoring](https://www.etsu.edu/uged/cfaa/learning/individualtutoring.php)
- Groups
    + [Supplemental Instruction](https://www.etsu.edu/uged/cfaa/learning/grouptutoring.php)
    + [Study Tables](https://www.etsu.edu/uged/cfaa/learning/studytables.php)

**[CFAA Tutoring FAQ](https://www.etsu.edu/uged/cfaa/learning/faq.php)**

**Location:** Sherrod Library, 1st Floor, adjacent to Einstein Bros. Bagels

### Undergraduate Student Success Specialist ###

https://www.etsu.edu/students/acts/usss.php

The Undergraduate Student Success Specialist (USSS) assists students with personal problems that may be interfering with their academic success. Often the USSS functions to guide a student to other existing university or community resources (advisement, tutoring, counselling, commuter/transportation services, etc.) 

From the webpage:

> The Undergraduate Student Success Specialist is a service to assist students with . . .      
> 
> 1. Setting realistic personal goals
> 2. Managing obstacles you may be having in day-to-day life
> 3. Resolving problems and providing support
> 4. Finding services on campus and in the community

**Location:** Sherrod Library, Room 452

### Counseling Center ###

https://www.etsu.edu/students/counseling/default.php

> We provide a variety of personal services, including drop-in consultations, crisis referrals, ongoing individual and couples therapy, group therapy and psychiatry. We also provide outreach programs in mental health and wellness, as well as serve as a training facility for graduate students in counseling, social work and psychology.

Depending on the nature of a student's special needs / disability, access to counseling / psychiatry can be very important. (For example, Autism Spectrum Disorder has high comorbidity rates with other conditions like depression and anxiety.)

**Location:** D.P. Culp Center, 3rd Floor

(Moving in May as part of Culp Center reno)