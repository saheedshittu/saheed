https://www.ecommercetimes.com/story/67701.html

Author mentions social media marketing and the motivators associated with a particular campaign.

Notable motivators include: self-expression (could we relate this to Dr.Pittarese's motivator self-esteem?),
	self-serving, altruism, and status achievement.

These motivators seem to be more on the observer's side, not ETSU's side, but nonetheless can provide a platform
from the opposite direction that we can attribute to the motivators mentioned in Pittarese's talk: greed, self-esteem,
and avoiding boredom.